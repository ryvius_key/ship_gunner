package main

import (
    "os"
    //"fmt"
    "bufio"
    "bytes"
    "strings"
)

func getLocale(msg map[string]string){
    s:=os.Getenv("LANG")
    //fmt.Println("LANG : ",s)
    f,err := os.Open("data/locale/"+s)
    if err!=nil{
        switch s[0:3]{
            case "zh_":
                msg["title"] = "摧毁敌舰"
                msg["hit"] = "击中目标"
                msg["miss"] = "没有击中"
                msg["damage"] = "摧毁目标"
                msg["long_damage"] = "目标已经被摧毁"
                msg["ready_fire"] = "可以射击"
                msg["loading"] = "正在装填炮弹"
                msg["count_fire"] = "主炮射击：%d 次"
                msg["count_hit"] = "命中次数：%d 次，破坏率：%.1f"
                msg["distance"] = "目标距离:%.0f M"
                msg["win"] = "窗口"
                msg["fs"] = "全屏"
                msg["quit"] = "退出"
                msg["fail"] = "任务失败"
                msg["success"] = "你成功于 %.0f 米外摧毁敌舰！"
                msg["fire_dist"] = "射击%.0f米外的敌人"
            default:
                msg["title"] = "Destroy enemy ship"
                msg["hit"] = "hit"
                msg["miss"] = "miss"
                msg["damage"] = "damage"
                msg["long_damage"] = "Target Damaged"
                msg["ready_fire"] = "Ready to Fire"
                msg["loading"] = "Loading Cannon Ball"
                msg["count_fire"] = "Gun Fire Count：%d"
                msg["count_hit"] = "Hit Count:%d , Damage:%.1f"
                msg["distance"] = "Target Distance:%.0f M"
                msg["win"] = "Win"
                msg["fs"] = "Full"
                msg["quit"] = "Quit"
                msg["fail"] = "Mission Failed"
                msg["success"] = "You dammage target beyound %.0f meters!"
                msg["fire_dist"] = "Enemy Dist:%.0f M"
        }
    }else {
        defer f.Close()
        reader1 := bufio.NewReader(f)
        var line1 []byte
        b:=new(bytes.Buffer)
        var pair []string
        for{
            b.Reset()
            line1,_,err = reader1.ReadLine()
            if err!=nil{
                break
            }
            b.Write(line1)
            pair = strings.SplitN(b.String(),":",2)
            msg[pair[0]] = pair[1]
        }
    }
}
